package com.endava.internship.mocking.service;

import com.endava.internship.mocking.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.UUID;
import java.util.stream.Stream;

import static com.endava.internship.mocking.model.Status.ACTIVE;
import static com.endava.internship.mocking.model.Status.INACTIVE;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;


class BasicValidationServiceTest {

    // initialization with null values for readability
    private final static Double validAmountValue = 52.31D;
    private final static UUID invalidUUID = null;
    private final static UUID validUUID = UUID.randomUUID();
    private final static Integer validUserId = 5;
    private final static Integer invalidUserId = null;
    private final static String validMessage = "example of correct message";
    private final static String invalidMessage = null;
    private final static String validNameForUser = "John";
    private final static User validUser = new User(validUserId, validNameForUser, ACTIVE);
    private final static User invalidUserOnStatus = new User(validUserId, validNameForUser, INACTIVE);

    private final ValidationService validationService = new BasicValidationService();

    @ParameterizedTest
    @MethodSource("sourceForValidationAmountTest")
    public void shouldThrowIllegalArgumentExceptionOnValidationAmount(Double comparableAmount, String exceptedMessage) {
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> validationService.validateAmount(comparableAmount)).getMessage();
        assertEquals(exceptedMessage, actualMessage);
    }

    @Test
    public void shouldNotThrowAnyExceptionOnValidationAmount() {
        assertDoesNotThrow(() -> validationService.validateAmount(validAmountValue));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnValidationPaymentId() {
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> validationService.validatePaymentId(invalidUUID)).getMessage();
        assertEquals("Payment id must not be null", actualMessage);
    }

    @Test
    public void shouldNotThrowAnyExceptionOnValidationPaymentId() {
        assertDoesNotThrow(() -> validationService.validatePaymentId(validUUID));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnValidationUserId() {
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> validationService.validateUserId(invalidUserId)).getMessage();
        assertEquals("User id must not be null", actualMessage);
    }

    @Test
    public void shouldNotThrowAnyExceptionOnValidationUserId() {
        assertDoesNotThrow(() -> validationService.validateUserId(validUserId));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnValidationMessage() {
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> validationService.validateMessage(invalidMessage)).getMessage();
        assertEquals("Payment message must not be null", actualMessage);
    }

    @Test
    public void shouldNotThrowAnyExceptionOnValidationMessage(){
        assertDoesNotThrow(() -> validationService.validateMessage(validMessage));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnValidationUser(){
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> validationService.validateUser(invalidUserOnStatus)).getMessage();
        assertEquals("User with id " + invalidUserOnStatus.getId() + " not in ACTIVE status", actualMessage);
    }

    @Test
    public void shouldNotThrowAnyExceptionOnValidationUser(){
        assertDoesNotThrow(() -> validationService.validateUser(validUser));
    }

    public static Stream<Arguments> sourceForValidationAmountTest() {
        return Stream.of(
                Arguments.of(null, "Amount must not be null"),
                Arguments.of(0D, "Amount must be greater than 0"),
                Arguments.of(-1.1D, "Amount must be greater than 0")
        );
    }

}