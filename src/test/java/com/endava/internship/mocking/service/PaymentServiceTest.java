package com.endava.internship.mocking.service;

import com.endava.internship.mocking.model.Payment;
import com.endava.internship.mocking.model.User;
import com.endava.internship.mocking.repository.PaymentRepository;
import com.endava.internship.mocking.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static com.endava.internship.mocking.model.Status.ACTIVE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {

    private static final Integer userId = 1;

    private static final Double amount = 1.3;

    private static final String nameUser = "John";

    @Mock
    private UserRepository userRepository;

    @Mock
    private PaymentRepository paymentRepository;

    @Mock
    private ValidationService validationService;

    @InjectMocks
    private PaymentService paymentService;


    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(userRepository, paymentRepository, validationService);
    }


    @Test
    public void shouldCreateNewPaymentSuccessfully() {
        final ArgumentCaptor<Payment> paymentArgumentCaptor = ArgumentCaptor.forClass(Payment.class);

        final Payment payment = new Payment(userId, amount, "Payment from user " + nameUser);
        final User user = new User(userId, nameUser, ACTIVE);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(paymentRepository.save(paymentArgumentCaptor.capture())).thenReturn(payment);

        final Payment actualPayment = paymentService.createPayment(userId, amount);

        assertEquals(payment, actualPayment);

        verify(validationService).validateUserId(userId);
        verify(validationService).validateAmount(amount);
        verify(validationService).validateUser(user);
        verify(userRepository).findById(userId);

        assertEquals(payment.getUserId(), paymentArgumentCaptor.getValue().getUserId());
        assertEquals(payment.getAmount(), paymentArgumentCaptor.getValue().getAmount());
        assertEquals(payment.getMessage(), paymentArgumentCaptor.getValue().getMessage());
    }

    @Test
    public void shouldThrowExceptionOnCreateNewPayment() {
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        final String error = assertThrows(NoSuchElementException.class, () -> paymentService.createPayment(userId, amount)).getMessage();

        assertEquals("User with id " + userId + " not found", error);

        verify(validationService).validateUserId(userId);
        verify(validationService).validateAmount(amount);
        verify(userRepository).findById(userId);
    }

    @Test
    public void shouldReturnEqualPaymentButWithDifferentMessageOnEditing(){
        final Payment expectedPayment = new Payment(userId, amount, "Payment from user " + nameUser);
        final UUID paymentID = expectedPayment.getPaymentId();
        final String newMessage = "new message for " + nameUser;

        final Payment returnedPayment = Payment.copyOf(expectedPayment);

        returnedPayment.setMessage(newMessage);

        when(paymentRepository.editMessage(paymentID, newMessage)).thenReturn(returnedPayment);

        final Payment actualPayment = paymentService.editPaymentMessage(paymentID, newMessage);

        assertEquals(expectedPayment, actualPayment);
        assertNotEquals(expectedPayment.getMessage(), actualPayment.getMessage());

        verify(validationService).validatePaymentId(paymentID);
        verify(validationService).validateMessage(newMessage);

    }

    @ParameterizedTest
    @MethodSource("sourceForComparingTest")
    public void shouldFilterPaymentsByAmountWherePaymentAmountMoreThanGivenAmount(List<Payment> paymentList, List<Payment> exceptedPayments, Double amountForComparing){
        when(paymentRepository.findAll()).thenReturn(paymentList);

        List<Payment> actualPayments = paymentService.getAllByAmountExceeding(amountForComparing);

        assertEquals(exceptedPayments, actualPayments);

        verify(paymentRepository).findAll();
    }

    public static Stream<Arguments> sourceForComparingTest(){
        List<Payment> paymentList = getPaymentsList();
        return Stream.of(
                Arguments.of(paymentList, Arrays.asList(paymentList.get(1), paymentList.get(3)), 2D),
                Arguments.of(paymentList, paymentList, 0D),
                Arguments.of(paymentList, Collections.singletonList(paymentList.get(3)), 8D)
        );
    }

    public static List<Payment> getPaymentsList(){
        return Arrays.asList(
                new Payment(1, 1.3D, "message for 1"),
                new Payment(2, 5.2D, "message for 2"),
                new Payment(3, 0.2D, "message for 3"),
                new Payment(4, 8.2D, "message for 4")
        );
    }
}

