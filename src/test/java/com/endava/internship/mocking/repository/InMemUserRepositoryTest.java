package com.endava.internship.mocking.repository;

import com.endava.internship.mocking.model.Status;
import com.endava.internship.mocking.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InMemUserRepositoryTest {

    private UserRepository userRepository;
    private final static Integer userId = 32;


    @BeforeEach
    public void setUp() {
        userRepository = new InMemUserRepository();
    }

    @Test
    public void shouldReturnUserOnFindingById() {
        final User expectedUser = new User(1, "John", Status.ACTIVE);

        final User actualUser = userRepository.findById(expectedUser.getId()).get();

        assertEquals(expectedUser, actualUser);
    }

    @Test
    public void shouldReturnNullOnFindingByIdWhereIdIsUnknownForLocalList(){
        final Optional<User> actualUser = userRepository.findById(userId);
        assertEquals(Optional.empty(), actualUser);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnFindingByIdWhereIdIsNull(){
        final String actualString = assertThrows(IllegalArgumentException.class, () -> userRepository.findById(null).get()).getMessage();
        assertEquals("User id must not be null", actualString);
    }

}
