package com.endava.internship.mocking.repository;

import com.endava.internship.mocking.model.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InMemPaymentRepositoryTest {

    private PaymentRepository paymentRepository;

    private static final Payment emptyPayment = null;
    private static final UUID emptyUUID = null;
    private static final Payment tempPayment = new Payment(10, 1D, "message");

    @BeforeEach
    public void setUp() {
        paymentRepository = new InMemPaymentRepository();
    }

    @Test
    public void shouldSavePaymentInLocalMapAndReturnSavedPaymentAndCheckForExistingInLocalMap() {
        final Payment expectedPayment = new Payment(1, 1.1D, "Message for 1 payment");

        final Payment actualPayment = paymentRepository.save(expectedPayment);
        assertEquals(expectedPayment, actualPayment);

    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnSavingPaymentWithNullValue() {
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> paymentRepository.save(emptyPayment)).getMessage();
        assertEquals("Payment must not be null", actualMessage);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnSavingExistingPayment() {
        final Payment actualPayment = paymentRepository.save(tempPayment);

        String actualMessage = assertThrows(IllegalArgumentException.class, () -> paymentRepository.save(tempPayment)).getMessage();
        assertEquals("Payment with id " + actualPayment.getPaymentId() + "already saved", actualMessage);
    }

    @Test
    public void shouldReturnCopyOfPaymentOnFindingById() {
        final Payment expectedPayment = new Payment(1, 1.1D, "Message for 1 payment");
        paymentRepository.save(expectedPayment);

        final Payment actualPayment = paymentRepository.findById(expectedPayment.getPaymentId()).get();

        assertEquals(expectedPayment, actualPayment);
        assertNotSame(expectedPayment, actualPayment);

    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnFindingPaymentByIdWhereIdIsNull(){
        String actualMessage = assertThrows(IllegalArgumentException.class, () -> paymentRepository.findById(emptyUUID)).getMessage();
        assertEquals("Payment id must not be null", actualMessage);
    }


    // question
    @Test
    public void shouldReturnNullValueOnFindingPaymentByIdWherePaymentWithGivenIdNotExist(){
        assertEquals(emptyPayment, paymentRepository.findById(tempPayment.getPaymentId()).orElse(emptyPayment));
    }

    @Test
    public void shouldReturnAllPaymentsFromLocalMapOnFindingAllPayments(){
        final Map<UUID, Payment> expectedPaymentHashMap = new HashMap<>();

        getValidPayments().forEach(payment -> expectedPaymentHashMap.put(payment.getPaymentId(), payment));

        final List<Payment> expectedPayments = new ArrayList<>(expectedPaymentHashMap.values());

        expectedPayments.forEach(payment -> paymentRepository.save(payment));

        final List<Payment> actualPayments = paymentRepository.findAll();

        assertEquals(expectedPayments, actualPayments);

    }

    @Test
    public void shouldReturnPaymentLikeGivenPaymentButWithAnotherMessageOnEditingMessage(){
        final Payment expectedPayment = paymentRepository.save(tempPayment);

        final Payment actualPayment = paymentRepository.editMessage(tempPayment.getPaymentId(), "new message");

        assertEquals(expectedPayment, actualPayment);
        assertNotEquals(expectedPayment.getMessage(), actualPayment.getMessage());
        assertNotSame(expectedPayment, actualPayment);

    }

    @Test
    public void shouldThrowIllegalArgumentExceptionOnEditingMessage(){
        final Payment paymentForAppending = new Payment(5, 2d, "random message for test");
        paymentRepository.save(paymentForAppending);

        final String actualMessage = assertThrows(NoSuchElementException.class, () -> paymentRepository.editMessage(tempPayment.getPaymentId(), "new message")).getMessage();

        assertFalse(paymentRepository.findAll().isEmpty());

        assertEquals("Payment with id " + tempPayment.getPaymentId() + " not found", actualMessage);
    }

    public static List<Payment> getValidPayments() {
        return Arrays.asList(
                new Payment(1, 1.1D, "Message for 1 payment"),
                new Payment(2, 4.1D, "Message for 2 payment"),
                new Payment(3, 4D, "Message for 3 payment")
        );
    }
}
